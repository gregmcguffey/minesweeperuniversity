# Design

The design will vary with language. Try to use the language's strengths.

The main guidance is to take an iterative approach. Each iteration should
provide some form of a complete _game_. 

## First Iteration

Create a minimal minesweeper. Cells can be empty, a mine or crater.
Triggering a mine ends the game. Skip timing, statistics. Depending on
language/framework, the UI can be anything from API calls to command line
to browser based to native app. But it should be simple. It should have
some way to show number of not marked.

## Second Iteration

Add ability to choose between easy, moderate, or hard levels.

## 3rd Iteration

Add ability to choose size and mine count.

## 4th Iteration

Add timer and statistics. This includes persisting statistics. Should
show elapsed time.

> You should have a clone of Windows Minesweeper at this point.

## 5th Iteration

Add game variations. I.e. provide different rule sets. E.g. allow more
than one life or auto open some part of the field. Allow user to select
between different variations. Update statistics to be per game variation.

## 6th Iteraion

Expand possibilities for game variations or improve UI.