# Requirements

One of the primary ideas with this learning project is to have a common,
known problem to use with every language. The problem is Minesweeper, the
classic Windows game. This problem has several aspects that allow learning
from the basics of a language, to the intricacies of a framework, to build
and deployment technics. This document defines the problem.

## Minesweeper Basics

The basics of the game are that there is a field that is a retangular area
comprised of cells. Each cell can either be empty or it can have a mine.
If the cell is empty, it will show how many adjacent cells have mines.
The user interacts with the field by clicking on cells. If they click on
a cell with a mine, the game ends and they have lost. If they expose every
cell that does not have a mine, they win.

## Game Components

The game is comprised of the following components:

* Field
* Game Engine
* UI

### Field

The field is a rectangular field comprised of cells. The cells can contains
some sort of object (with a mine being the most basic) or it can be empty.
Each cell can also either be exposed, indicating the contents can be viewed
or they can be buried, with the content not directly known (the game hinges
on the indirect knowledge of a cells content).

The field tracks both of these states (contents and exposure) and provides
the mechanism to transistion between states. The field does not impose any
rules about state transistion (that is the game engines job).

The field can also track information about a cell. the most obvious is
tracking how many adjacent cells have mines for an empty cell.

#### Field Factory

There must also be a mechanism to create a field. Field creation should be
configurable. Minimally, the size (height and width) and the number of mines
should be configurable. For more advanced scenarios, other configuration
could be possible, such as definining walls/rocks/items in the field.

### Game Engine

The game engine controls the game. It interacts with the field and provides
an API for the UI to interact with. It defines the rules of the game and
the win/lose scenarios.

The game engine allows you to learn more about software design principles,
in addition to learning a new language or framework.

#### Responsibilities

The game engine is responsible for:

* Initiating field construction with appropriate parameters (size and number
    of mines).
* Enforcing rules based on the state of the field.
* Determining game end based on the state of the field.

#### Basic Game Rules

The following define the basic game rules. This is the starting place for
any project.

1. Field is comprised of empty or mine cells.
1. The size of the field (height and width) and the number of mines is configurable.
1. Provides mechanism to expose a cell.
1. Provides mechanism to mark a cell as a possible mine.
1. Provides mechanism to mark a cell as a mine.
1. Provides mechanism to clear a marked cell.
1. A marked cell (either mark) cannot be exposed.
1. Provides mechanism to expose all non-marked cells adjacent to the the cell.
1. Game is lost if a mine cell is exposed.
1. Game is won if all empty cells are exposed.

### UI

The UI allows the user to start a game, then view and interact with the field.
It shows them if they lose or if they win. They can view statistics from
previous games.

The UI can be quite varied:

* Native app (as was the original game)
* An app store app (e.g. Universal App in Windows, or an iPhone or Android app)
* A web app
* A console app

Feel free to build many UIs for a game engine, as needed to learn new frameworks/
languages.

## Other Components

Independent of the game, there can be several other components: 

* Statistics
* Saved Games
* Game Variations

### Statistics

Statistics can be tracked for the game and persisted. The game will track results
of games and the statistics can be shown to the user.

This allows learning how to persist data. The following are some examples:

* Text file, with statistics in JSON or XML format.
* Database, with statistics in a table.
* In cloud, using any of the data persistance options.

### Saved Games

A game can be saved while in progress and later restored.

### Game Variations

This allows for different field configurations and/or rules to be used
for a game.