# README

Learning projects using Minesweeper.

In order to learn new languages, frameworks, libraries, etc., I use Minesweeper as a tool to learn.
Each major branch is focused on what I'm learning. The master branch contains the basics of the design
of Minesweeper, which is then used with each new learning project. These major branches are never 
intended to be merged. Rather they an archive of what I've figured out so far.

## Getting Setup

Each branch has a readme that describes how to setup for that particular language.

A common set of requirements are provided in a [requirements document](docs/requirements.md).

A set of design considerations are also provided in a [design document](docs/design.md).

Treat each primary language branch as the master for that language. It is a bad idea to try to
merge between languages =:-O

## Contribution guidelines

Since this is my learning project, no contributions. Feel free to fork this repo and use it
for your own learning though!
